## NPM Modules used

[Checkbox library](https://github.com/sconxu/react-native-checkbox)

[Numeric keypad](https://github.com/beefe/react-native-keyboard)

[Mobile phone number validation library](https://github.com/googlei18n/libphonenumber/blob/master/javascript/i18n/phonenumbers/demo.js)

[Native Navigation](https://github.com/airbnb/native-navigation)

[React Native Keychain](https://github.com/oblador/react-native-keychain)

## Running the app

```bash
npm install
npm start
```

To run the iOS App:

```bash
react-native run-ios
```

To run the Android App: (Currently not setup - libraries/npm modules need to be manually linked)

```bash
react-native run-android
```

## ISSUES

[npm postinstall script explained](https://github.com/facebook/react-native/issues/13198)

Because this is a custom RN project (rather than a `react-native init`), there were additional steps required to get things working properly[React Native assets config setup](https://stackoverflow.com/questions/35354998/react-native-ios-app-not-showing-static-assets-images-after-deploying)
[React Native jsbundle config](https://medium.com/react-native-development/deploying-a-react-native-app-for-ios-pt-1-a79dfd15acb8)


## REFERENCES
[Adding a splash screen to RN App](https://medium.com/handlebar-labs/how-to-add-a-splash-screen-to-a-react-native-app-ios-and-android-30a3cec835ae)

[Mobile phone number formatting example](https://github.com/googlei18n/libphonenumber#quick-examples)