// @flow
import React from 'react'
import { PhoneNumberUtil, PhoneNumberFormat } from 'google-libphonenumber'
import NumericKeyboardAndInput from '../components/NumericKeyboardAndInput'
import Navigator from 'native-navigation'
import Screen from '../components/Screen'
import Footer from '../components/Footer'
import Button from '../components/Button'
import Heading from '../components/Heading'
import Paragraph from '../components/Paragraph'
import * as MOBILENUMBER_COPY from '../constants/routes'
import { CARDREADER } from '../constants/routes'

const MobileNumber = () => {
  let mobileNumber = ''
  const phoneUtil = PhoneNumberUtil.getInstance()

  const onNext = () => {
    if (mobileNumber) {
      try {
        const phoneNumber = phoneUtil.parse(mobileNumber, 'GB')
        const isValid = phoneUtil.isValidNumber(phoneNumber)
        const isMobile = phoneUtil.getNumberType(phoneNumber) === 1

        if (isValid && isMobile) {
          donor.mobileNumber = phoneUtil.format(phoneNumber, PhoneNumberFormat.E164)
          Navigator.push(CARDREADER)
        } else {
          alert(MOBILENUMBER_COPY.MOBILENUMBER_INVALID)
        }
      } catch (error) {
        alert(MOBILENUMBER_COPY.MOBILENUMBER_INVALID)
      }
    } else {
      alert(MOBILENUMBER_COPY.MOBILENUMBER_ENTER)
    }
  }

  const onValueChange = (newValue) => {
    mobileNumber = newValue
    return mobileNumber
  }

  return (
    <Screen>
      <Heading text={ MOBILENUMBER_COPY.MOBILENUMBER_HEADING } />

      <Paragraph
        text={ MOBILENUMBER_COPY.MOBILENUMBER_PARAGRAPH } />

      <NumericKeyboardAndInput
        onValueChange={ onValueChange }
        type={ 'number-pad' } />

      <Footer>
        <Button
          title={ MOBILENUMBER_COPY.MOBILENUMBER_BUTTON }
          onPress={ onNext } />
      </Footer>
    </Screen>
  )
}

export default MobileNumber
