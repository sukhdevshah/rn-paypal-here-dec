// @flow
import React from 'react'
import { Text, View } from 'react-native'
import Checkbox from '../components/Checkbox'
import Navigator from 'native-navigation'
import Screen from '../components/Screen'
import Footer from '../components/Footer'
import Button from '../components/Button'
import Heading from '../components/Heading'
import Paragraph from '../components/Paragraph'
import { CARDREADER, MOBILENUMBER } from '../constants/routes'
import * as GIFTAID_COPY from '../constants/copy/giftaid'
import styles from '../styles/giftaid.css'

const GiftAid = () => {
  let giftAidCheckboxChecked = false

  const onNext = () => {
    let route = CARDREADER

    if ( giftAidCheckboxChecked ) {
      route = MOBILENUMBER
    }

    Navigator.push(route)
  }

  const onCheckboxChange = (newValue) => {
    donor.giftAid = newValue
    giftAidCheckboxChecked = newValue
    return giftAidCheckboxChecked
  }

  return (
    <Screen>
      <Heading text={ GIFTAID_COPY.GIFTAID_HEADING } />

      <Paragraph
        text={ GIFTAID_COPY.GIFTAID_PARAGRAPH(donor.amount) } />

      <View style={ styles.giftAidContainer }>
        <Checkbox onChange={ onCheckboxChange } />

        <Text style={ styles.disclaimerText }>
          { GIFTAID_COPY.GIFTAID_DISCLAIMER }
        </Text>
      </View>

      <Footer>
        <Button
          title={ GIFTAID_COPY.GIFTAID_BUTTON }
          onPress={ onNext } />
      </Footer>
    </Screen>
  )
}

export default GiftAid
