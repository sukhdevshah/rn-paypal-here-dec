// @flow
import React from 'react'
import { NativeModules } from 'react-native'
import NumericKeyboardAndInput from '../components/NumericKeyboardAndInput'
import Navigator from 'native-navigation'
import Screen from '../components/Screen'
import Footer from '../components/Footer'
import Button from '../components/Button'
import Heading from '../components/Heading'
import Paragraph from '../components/Paragraph'
import { PAYPAL_INVOICE_NAME } from '../constants'
import { GIFTAID } from '../constants/routes'
import * as MANUALAMOUNTSELECTION_COPY from '../constants/copy/manualamountselection'

const { PayPalHere } = NativeModules

const ManualAmountSelection = () => {
  let value = ''

  const onManualAmountEnter = () => {
    if (value && parseInt(value) > 0) {
      donor.amount = value
      PayPalHere.setupReader(PAYPAL_INVOICE_NAME, value)
      Navigator.push(GIFTAID)
    } else {
      alert('Please enter the amount you wish to donate')
    }
  }

  const onValueChange = (newValue) => {
    value = newValue
    return value
  }

  return (
    <Screen>
      <Heading text={ MANUALAMOUNTSELECTION_COPY.MANUALAMOUNTSELECTION_HEADING } />

      <Paragraph
        text={ MANUALAMOUNTSELECTION_COPY.MANUALAMOUNTSELECTION_PARAGRAPH } />

      <NumericKeyboardAndInput
        prefixText={ '£' }
        onValueChange={ onValueChange }
        type={ 'decimal-pad' } />

      <Footer>
        <Button
          title={ MANUALAMOUNTSELECTION_COPY.MANUALAMOUNTSELECTION_BUTTON }
          onPress={ onManualAmountEnter } />
      </Footer>
    </Screen>
  )
}

export default ManualAmountSelection
