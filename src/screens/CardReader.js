// @flow
import React, { Component } from 'react'
import { NativeModules, Image, View, Text } from 'react-native'
import Navigator from 'native-navigation'
import { DONATION_POST } from '../api'
import Screen from '../components/Screen'
import Heading from '../components/Heading'
import Paragraph from '../components/Paragraph'
import Footer from '../components/Footer'
import { THANKYOU, CANCELLED } from '../constants/routes'
import * as CARDREADER_COPY from '../constants/copy/cardreader'
import styles from '../styles/cardreader.css'

const { PayPalHere } = NativeModules

class CardReader extends Component {
  state = {
    status: ''
  }

  constructor(props) {
    super(props)

    eventEmitter.addListener('PayPalPaymentResponseSuccess', (body) => {
      const donation = {
        ...donor,
        donation: body
      }

      delete donation.amount

      DONATION_POST(donation)
        .then(res => res.json())
        .then(Navigator.present(THANKYOU))
        .catch(error => alert(CARDREADER_COPY.CARDREADER_ERROR_POST(body.payPalInvoiceId)))
    })

    eventEmitter.addListener('PayPalPaymentResponseFailed', (body) => {
      Navigator.present(CANCELLED)
    })

    eventEmitter.addListener('PayPalPaymentReaderIdle', (body) => {
      this.setState({ status: 'Reader Idle' })
    })

    eventEmitter.addListener('PayPalPaymentGettingInfo', (body) => {
      this.setState({ status: 'Getting Info' })
    })

    eventEmitter.addListener('PayPalPaymentStartReaderDetection', (body) => {
      this.setState({ status: 'Start Reader Detection' })
    })

    eventEmitter.addListener('PayPalPaymentDidDetectReader', (body) => {
      this.setState({ status: 'Reader Detected' })
    })

    eventEmitter.addListener('PayPalPaymentReaderRemoved', (body) => {
      this.setState({ status: 'Reader removed' })
    })

    eventEmitter.addListener('PayPalPaymentCardReadBegun', (body) => {
      this.setState({ status: 'Card reader begun' })
    })

    eventEmitter.addListener('PayPalPaymentCardDataReceived', (body) => {
      this.setState({ status: 'Card Data Received' })
    })

    eventEmitter.addListener('PayPalPaymentReadCardNotAllowed', (body) => {
      this.setState({ status: 'Card Read Not Allowed' })
    })

    eventEmitter.addListener('PayPalPaymentFailedToReadCard', (body) => {
      this.setState({ status: 'Card Read Failed' })
    })

    eventEmitter.addListener('PayPalPaymentProcessing', (body) => {
      this.setState({ status: 'Payment Processing' })
    })

    eventEmitter.addListener('PayPalPaymentWaitingForSignature', (body) => {
      this.setState({ status: 'Waiting for Signature' })
    })

    eventEmitter.addListener('PayPalPaymentCancelled', (body) => {
      this.setState({ status: 'Payment Cancelled' })
    })

    eventEmitter.addListener('PayPalPaymentDeclined', (body) => {
      this.setState({ status: 'Payment Declined' })
    })
  }

  componentDidMount() {
    PayPalHere.enableContactlessReader()
  }

  render() {
    return (
      <Screen>
        <Heading text={ CARDREADER_COPY.CARDREADER_HEADING(donor.amount) } />

        <Paragraph
          text={ CARDREADER_COPY.CARDREADER_PARAGRAPH } />

        <Image
          source={ require('../images/reader.png') } />

        <Footer>
          <View style={ styles.logoContainer }>
            <Text style={ styles.centeredText }>Secured by PayPal</Text>
            <Image
              style={ styles.logo }
              source={ require('../images/paypal.png') } />
          </View>
        </Footer>
      </Screen>
    )
  }
}

export default CardReader
