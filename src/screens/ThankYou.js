// @flow
import React from 'react'
import Navigator from 'native-navigation'
import Screen from '../components/Screen'
import Footer from '../components/Footer'
import Button from '../components/Button'
import Heading from '../components/Heading'
import Paragraph from '../components/Paragraph'
import { AMOUNTSELECTION } from '../constants/routes'
import * as THANKYOU_COPY from '../constants/copy/thankyou'

const ThankYou = () => {
  const onNewDonation = () => {
    Navigator.present(AMOUNTSELECTION)
  }

  return (
    <Screen>
      <Heading text={ THANKYOU_COPY.THANKYOU_HEADING } />

      <Paragraph
        text={ THANKYOU_COPY.THANKYOU_PARAGRAPH } />

      <Footer>
        <Button
          title={ THANKYOU_COPY.THANKYOU_BUTTON }
          onPress={ onNewDonation } />
      </Footer>
    </Screen>
  )
}

export default ThankYou
