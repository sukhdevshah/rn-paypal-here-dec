// @flow
import React from 'react'
import { NativeModules, View } from 'react-native'
import Navigator from 'native-navigation'
import Screen from '../components/Screen'
import Footer from '../components/Footer'
import Button from '../components/Button'
import Heading from '../components/Heading'
import Paragraph from '../components/Paragraph'
import { PAYPAL_INVOICE_NAME } from '../constants'
import { MANUALAMOUNTSELECTION, GIFTAID } from '../constants/routes'
import * as AMOUNTSELECTION_COPY from '../constants/copy/amountselection'
import styles from '../styles/amountselection.css'

const { PayPalHere } = NativeModules

const AmountSelection = () => {
  const onAmountSelect = (amount) => {
    donor.amount = amount
    PayPalHere.setupReader(PAYPAL_INVOICE_NAME, amount)
    Navigator.push(GIFTAID)
  }

  const onEnterManual = () => {
    Navigator.push(MANUALAMOUNTSELECTION)
  }

  return (
    <Screen>
      <Heading text={ AMOUNTSELECTION_COPY.AMOUNTSELECTION_HEADING } />

      <Paragraph
        text={ AMOUNTSELECTION_COPY.AMOUNTSELECTION_PARAGRAPH } />

      <View style={ styles.amountContainer }>
        <Button
          style={ styles.amountButton }
          title={ '£3' }
          type={ 'secondary' }
          onPress={ () => onAmountSelect('3.00') } />
        <Button
          style={ styles.amountButton }
          title={ '£5' }
          type={ 'secondary' }
          onPress={ () => onAmountSelect('5.00') } />
        <Button
          style={ styles.amountButton }
          title={ '£10' }
          type={ 'secondary' }
          onPress={ () => onAmountSelect('10.00') } />
        <Button
          style={ styles.amountButton }
          title={ '£20' }
          type={ 'secondary' }
          onPress={ () => onAmountSelect('20.00') } />
        <Button
          style={ styles.amountButton }
          title={ '£30' }
          type={ 'secondary' }
          onPress={ () => onAmountSelect('30.00') } />
      </View>

      <Footer>
        <Button
          title={ AMOUNTSELECTION_COPY.AMOUNTSELECTION_CHOOSEAMOUNT }
          onPress={ onEnterManual } />
      </Footer>
    </Screen>
  )
}

export default AmountSelection
