// @flow
import React, { Component } from 'react'
import { TextInput, NativeModules } from 'react-native'
// import * as Keychain from 'react-native-keychain'
import { setAccessToken, LOGIN_POST, REFRESH_URL_GET } from '../api'
import { SCREEN_HEIGHT } from '../constants'
import Navigator from 'native-navigation'
import Screen from '../components/Screen'
import Footer from '../components/Footer'
import Button from '../components/Button'
import Paragraph from '../components/Paragraph'
import CustomSwitch from '../components/CustomSwitch'
import { AMOUNTSELECTION } from '../constants/routes'
import globalStyles from '../styles/styles.css'
import * as LOGIN_COPY from '../constants/copy/login'
import { COLOR_TEXT_PLACEHOLDER } from '../constants/colors'

const { PayPalHere } = NativeModules

class Login extends Component {
  state = {
    isLoading: false,
    username: '',
    password: '',
    sandbox: true,
  }

  componentDidMount() {
    eventEmitter.addListener('PayPalInitSuccess', () => {
      this.setState({ isLoading: false })
      Navigator.push(AMOUNTSELECTION)
    })

    eventEmitter.addListener('PayPalInitFail', (body) => {
      this.setState({ isLoading: false })
      alert(body)
    })

    eventEmitter.addListener('PayPalInitInsufficientCredentials', (body) => {
      this.setState({ isLoading: false })
      alert(body)
    })

    // Keychain.resetGenericPassword()
    //   .then(function() {
    //     console.log('Credentials successfully deleted');
    //   })

    // Keychain.getGenericPassword()
    //   .then(credentials => {
    //     if (credentials.username) {
    //       this.setState({
    //         username: credentials.username,
    //         password: credentials.password
    //       })
    //
    //       this.onLogin()
    //     }
    //   })
    //   .catch(error => {
    //     console.log('Keychain couldn\'t be accessed! Maybe no value set?', error)
    //   })
  }

  setSandbox() {
    global.sandbox = true
    PayPalHere.setSandboxEnvironment()
  }

  onLogin = async () => {
    const username = this.state.username
    const password = this.state.password

    if (username && password) {
      this.setState({ isLoading: true })

      if (this.state.sandbox) {
        this.setSandbox()
      }

      const credentials = {
        user: username.toLowerCase(),
        pass: password
      }

      try {
        this.setState({ status: LOGIN_COPY.LOGIN_GETTING_ACCESS_TOKEN })

        let loginRequest = await LOGIN_POST(credentials)
        let loginResponse = await loginRequest.json()

        setAccessToken(loginResponse.access_token)

        if (loginResponse) {
          try {
            this.setState({ status: LOGIN_COPY.LOGIN_GETTING_REFRESH_URL })

            let authRequest = await REFRESH_URL_GET()
            let authResponse = await authRequest.json()

            if (authResponse) {
              try {
                this.setState({ status: LOGIN_COPY.LOGIN_GETTING_PAYPAL_ACCESS_TOKEN })

                let setupRequest = await fetch(authResponse.current)
                let setupResponse = await setupRequest.json()

                this.setState({ status: LOGIN_COPY.LOGIN_SETTING_UP_PAYPAL_HERE_READER })

                PayPalHere.setupWithCredentials(setupResponse.access_token, authResponse.current, setupResponse.expires_in)

                // Keychain.setGenericPassword(username, password)
              } catch (error) {
                alert(LOGIN_COPY.LOGIN_PAYPAL_REQUIRED_FAILED)
              }
            }
          } catch (error) {
            alert(LOGIN_COPY.LOGIN_GETTING_REFRESH_URL_FAILED)
          }
        }
      } catch (error) {
        alert(LOGIN_COPY.LOGIN_ENTER_VALID_CREDENTIALS)
      }

      this.setState({ isLoading: false })
    } else {
      alert(LOGIN_COPY.LOGIN_ENTER_CREDENTIALS)
    }
  }

  onSwitchChange = (value) => {
    this.setState({ sandbox: value })
  }

  render() {
    return (
      <Screen
        status={ this.state.status }
        isLoading={ this.state.isLoading }>

        <Paragraph
          text={ LOGIN_COPY.LOGIN_TEXT } />

        <TextInput
          style={ globalStyles.textInput }
          placeholder={ LOGIN_COPY.LOGIN_PLACEHOLDER_USERNAME }
          placeholderTextColor={ COLOR_TEXT_PLACEHOLDER }
          onChangeText={ text => this.setState({ username: text }) }
          value={ this.state.username } />

        <TextInput
          secureTextEntry
          style={ globalStyles.textInput }
          placeholder={ LOGIN_COPY.LOGIN_PLACEHOLDER_PASSWORD }
          placeholderTextColor={ COLOR_TEXT_PLACEHOLDER }
          onChangeText={ text => this.setState({ password: text }) }
          value={ this.state.password } />

        <CustomSwitch
          onSwitchChange={ (value) => { this.onSwitchChange(value) } }
          value={ this.state.sandbox } />

        <Footer>
          <Button
            title={ LOGIN_COPY.LOGIN_BUTTON_TEXT }
            onPress={ this.onLogin } />
        </Footer>
      </Screen>
    )
  }
}

export default Login
