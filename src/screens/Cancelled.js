// @flow
import React from 'react'
import Navigator from 'native-navigation'
import Screen from '../components/Screen'
import Footer from '../components/Footer'
import Button from '../components/Button'
import Heading from '../components/Heading'
import Paragraph from '../components/Paragraph'
import { AMOUNTSELECTION } from '../constants/routes'
import * as CANCELLED_COPY from '../constants/copy/cancelled'

const Cancelled = () => {
  const onNewDonation = () => {
    Navigator.present(AMOUNTSELECTION)
  }

  return (
    <Screen>
      <Heading text={ CANCELLED_COPY.CANCELLED_HEADING } />

      <Paragraph
        text={ CANCELLED_COPY.CANCELLED_PARAGRAPH } />

      <Footer>
        <Button
          title={ CANCELLED_COPY.CANCELLED_BUTTON }
          onPress={ onNewDonation } />
      </Footer>
    </Screen>
  )
}

export default Cancelled
