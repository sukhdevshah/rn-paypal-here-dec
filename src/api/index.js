import { config } from '../config'

export function setAccessToken(accessToken) {
  return global.accessToken = accessToken
}

export function LOGIN_POST(credentials) {
  return fetch(`${config.API_ROOT()}${config.LOGIN_URL}`, {
    headers: {
      'Cache-Control': 'no-cache',
      'Content-Type': 'application/json'
    },
    method: 'POST',
    body: JSON.stringify(credentials)
  })
}

export function REFRESH_URL_GET() {
  return fetch(`${config.API_ROOT()}${config.REFRESH_URL}`, {
    method: 'GET',
    headers: {
      'Authorization': `Bearer ${accessToken}`
    }
  })
}

export function DONATION_POST(data) {
  return fetch(`${config.API_ROOT()}${config.SAVE_DONATION_URL}`, {
    headers: {
      'Cache-Control': 'no-cache',
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${accessToken}`
    },
    method: 'POST',
    body: JSON.stringify(data)
  })
}