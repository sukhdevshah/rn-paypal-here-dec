import { SCREEN_HEIGHT } from '../constants'
import { COLOR_TEXT_PRIMARY } from '../constants/colors'

module.exports = {
  logoContainer: {
    flex: 1,
    alignSelf: 'center',
    height: SCREEN_HEIGHT / 12,
    marginBottom: SCREEN_HEIGHT / 30,
  },
  centeredText: {
    textAlign: 'center',
    marginBottom: SCREEN_HEIGHT / 150,
    color: COLOR_TEXT_PRIMARY,
    fontFamily: 'OpenSans',
  },
  logo: {
    flex: 1,
    resizeMode: 'contain',
  }
}
