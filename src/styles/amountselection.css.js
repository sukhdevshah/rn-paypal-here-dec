import { SCREEN_WIDTH } from '../constants'
import { COLOR_AMOUNT_BOX_BACKGROUND } from '../constants/colors'

module.exports = {
  amountContainer: {
    flexDirection: 'row',
    alignSelf: 'center',
  },
  amountButton: {
    width: SCREEN_WIDTH / 8,
    backgroundColor: COLOR_AMOUNT_BOX_BACKGROUND,
    paddingHorizontal: 0,
    marginHorizontal: SCREEN_WIDTH / 50,
  },
}
