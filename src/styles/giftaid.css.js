import { COLOR_GIFTAID_DISCLAIMER } from '../constants/colors'

module.exports = {
  giftAidContainer: {
    width: '70%',
  },
  disclaimerText: {
    textAlign: 'center',
    color: COLOR_GIFTAID_DISCLAIMER,
    fontFamily: 'OpenSans',
  },
}
