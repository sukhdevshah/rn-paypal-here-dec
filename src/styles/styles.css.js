import { SCREEN_WIDTH, SCREEN_HEIGHT } from '../constants'
import { COLOR_TEXT_PRIMARY, COLOR_BOUNDARY_BOX } from '../constants/colors'

const textInput = {
  fontSize: SCREEN_HEIGHT / 18,
  marginVertical: 10,
  textAlign: 'center',
  borderStyle: 'solid',
  borderWidth: 1,
  borderColor: COLOR_BOUNDARY_BOX,
  paddingVertical: SCREEN_HEIGHT / 50,
  paddingHorizontal: SCREEN_WIDTH / 15,
  backgroundColor: 'transparent',
  color: COLOR_TEXT_PRIMARY,
  fontFamily: 'OpenSans',
}

module.exports = {
  textInput,
  textContainer: {
    ...textInput,
    height: SCREEN_HEIGHT / 11,
    paddingVertical: SCREEN_HEIGHT / 70,
  },
}
