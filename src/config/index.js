import { DEV_URL, PROD_URL } from '../constants/api'

export const config = {
  API_ROOT: function() {
    return (global.sandbox ? DEV_URL : PROD_URL)
  },
  LOGIN_URL: '/login',
  REFRESH_URL: '/currentRefresh',
  SAVE_DONATION_URL: '/saveDonation',
}
