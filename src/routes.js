// @flow
import { NativeEventEmitter, NativeModules } from 'react-native'
import Navigator from 'native-navigation'
import * as ROUTE from './constants/routes'
import { removeReaderListeners } from './functions'

const { PayPalHere } = NativeModules

global.donor = {}
global.eventEmitter = new NativeEventEmitter(PayPalHere)

PayPalHere.askForLocationAccess()

Navigator.registerScreen(ROUTE.LOGIN, () => require('./screens/Login'))
Navigator.registerScreen(ROUTE.AMOUNTSELECTION, () => require('./screens/AmountSelection'))
Navigator.registerScreen(ROUTE.MANUALAMOUNTSELECTION, () => require('./screens/ManualAmountSelection'))
Navigator.registerScreen(ROUTE.MOBILENUMBER, () => require('./screens/MobileNumber'))
Navigator.registerScreen(ROUTE.GIFTAID, () => require('./screens/GiftAid'))
Navigator.registerScreen(ROUTE.CARDREADER, () => require('./screens/CardReader'))
Navigator.registerScreen(ROUTE.THANKYOU, () => {
  removeReaderListeners()
  return require('./screens/ThankYou')
})
Navigator.registerScreen(ROUTE.CANCELLED, () => {
  removeReaderListeners()
  return require('./screens/Cancelled')
})
