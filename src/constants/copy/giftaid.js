export const GIFTAID_HEADING = `Why not Gift Aid your donation?`
export function GIFTAID_PARAGRAPH(amount) {
  return `You can add £${ ((amount / 100) * 25).toFixed(2) } to your donation at no extra cost to you by allowing us to claim Gift Aid on your donation. Would you be happy to do this?`
}
export const GIFTAID_DISCLAIMER = `I am a UK taxpayer and understand that if I pay less Income Tax and/or Capital Gains Tax than the amount of Gift Aid claimed on all my donations or membership subscriptions in that tax year it is my responsibility to pay any difference.`
export const GIFTAID_BUTTON = `Next`

export const GIFTAID_CHECKBOX_COPY = `Yes, I would like DEC to reclaim Gift Aid on this donation and any donations that I have made over the last four years and in the future, until I notify you otherwise.`
