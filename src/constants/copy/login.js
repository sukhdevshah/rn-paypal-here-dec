export const LOGIN_TEXT = `Please login`
export const LOGIN_PLACEHOLDER_USERNAME = `USERNAME`
export const LOGIN_PLACEHOLDER_PASSWORD = `PASSWORD`
export const LOGIN_BUTTON_TEXT = `Login`

export const LOGIN_GETTING_ACCESS_TOKEN = `Getting access token`
export const LOGIN_GETTING_REFRESH_URL = `Getting refresh URL`
export const LOGIN_GETTING_PAYPAL_ACCESS_TOKEN = `Getting PayPal access token`
export const LOGIN_SETTING_UP_PAYPAL_HERE_READER = `Setting up PayPal Here Reader`

export const LOGIN_PAYPAL_REQUIRED_FAILED = `Getting required variables for PayPal SDK failed`
export const LOGIN_GETTING_REFRESH_URL_FAILED = `Getting refresh url failed`
export const LOGIN_ENTER_VALID_CREDENTIALS = `Please enter a valid username and password`
export const LOGIN_ENTER_CREDENTIALS = `Please enter your username and password`
