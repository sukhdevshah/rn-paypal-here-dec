export const MOBILENUMBER_HEADING = `Completing your Gift Aid declaration`
export const MOBILENUMBER_PARAGRAPH = `Please enter your mobile number here so that we can send you a text link explaining how to complete your Gift Aid declaration.`
export const MOBILENUMBER_BUTTON = `Donate now`

export const MOBILENUMBER_INVALID = `Invalid UK Mobile Number entered`
export const MOBILENUMBER_ENTER = `Please enter your UK Mobile Number`
