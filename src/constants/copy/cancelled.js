export const CANCELLED_HEADING = `Sorry!`
export const CANCELLED_PARAGRAPH = `We were unable to process your payment.`
export const CANCELLED_BUTTON = `Please try again`
