export const THANKYOU_HEADING = `We’re all done!`
export const THANKYOU_PARAGRAPH = `Thank you again for your generosity.`
export const THANKYOU_BUTTON = `New donation`
