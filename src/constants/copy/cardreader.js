export function CARDREADER_HEADING(amount) {
  return `Amount to pay: £${amount}`
}
export const CARDREADER_PARAGRAPH = `Please make your secure payment using the PayPal Here device.`
export function CARDREADER_ERROR_POST(invoiceId) {
  return `Networking issue for InvoiceId: ${invoiceID}. Payment has been taken but Gift Aid SMS could not be sent if requested.`
}
