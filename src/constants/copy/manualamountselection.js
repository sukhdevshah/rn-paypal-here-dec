export const MANUALAMOUNTSELECTION_HEADING = `Your donation`
export const MANUALAMOUNTSELECTION_PARAGRAPH = `How much would you like to give?`
export const MANUALAMOUNTSELECTION_BUTTON = `Next`
