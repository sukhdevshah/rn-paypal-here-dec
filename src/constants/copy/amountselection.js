export const AMOUNTSELECTION_HEADING = `Thank you for agreeing to donate to our emergency appeal`
export const AMOUNTSELECTION_PARAGRAPH = `Please select how much you’d like to give`
export const AMOUNTSELECTION_CHOOSEAMOUNT = `Or choose your own amount`
