import { Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window')

export const PAYPAL_INVOICE_NAME = 'Kew Donation'
export const SCREEN_WIDTH = width
export const SCREEN_HEIGHT = height