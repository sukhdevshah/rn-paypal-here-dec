import React from 'react'
import PropTypes from 'prop-types'
import { View, ActivityIndicator, Text } from 'react-native'

const propTypes = {
  status: PropTypes.string,
}

const Loading = ({ status }) =>
  <View style={ styles.container }>
    <ActivityIndicator size={ 'large' } />
    { status && <Text style={ styles.status }>{ status }</Text> }
  </View>

const styles = {
  container: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#000',
    opacity: 0.7,
    flex: 1,
    zIndex: 99,
  },
  status: {
    color: '#fff',
    marginTop: 10,
    fontFamily: 'OpenSans',
  }
}

Loading.propTypes = propTypes

export default Loading
