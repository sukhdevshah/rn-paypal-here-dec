import React, { Component } from 'react'
import { TouchableWithoutFeedback, Text, View } from 'react-native'
import CheckBox from 'react-native-checkbox'
import PropTypes from 'prop-types'
import { SCREEN_WIDTH, SCREEN_HEIGHT } from '../constants'
import { COLOR_TEXT_PRIMARY, COLOR_BOUNDARY_BOX } from '../constants/colors'
import * as GIFTAID_COPY from '../constants/copy/giftaid'

class Checkbox extends Component {
  state = {
    value: false
  }

  static propTypes = {
    onChange: PropTypes.func.isRequired,
  }

  onCheckboxChange = () => {
    this.setState({ value: !this.state.value }, this.onChange)
  }

  onChange = () => {
    this.props.onChange(this.state.value)
  }

  render() {
    return (
      <TouchableWithoutFeedback onPress={this.onCheckboxChange}>
        <View style={ styles.container }>
          <CheckBox
            label={ '' }
            checked={ this.state.value }
            checkboxStyle={ styles.checkbox }
            uncheckedImage={ require('../images/empty-checkbox.png') }
            checkedImage={ require('../images/check-mark.png') }
            onChange={ this.onCheckboxChange } />

          <Text style={ styles.label }>
            { GIFTAID_COPY.GIFTAID_CHECKBOX_COPY }
          </Text>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

const styles = {
  container: {
    flexDirection: 'row',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: COLOR_BOUNDARY_BOX,
    paddingHorizontal: SCREEN_WIDTH / 41,
    paddingVertical: SCREEN_HEIGHT / 20,
    marginVertical: SCREEN_HEIGHT / 20,
    width: '100%',
    alignItems: 'center',
  },
  checkbox: {
    height: SCREEN_HEIGHT / 25,
    width: SCREEN_HEIGHT / 25,
  },
  label: {
    flex: 1,
    fontSize: SCREEN_HEIGHT / 47,
    color: COLOR_TEXT_PRIMARY,
    fontFamily: 'OpenSans',
  },
}

export default Checkbox
