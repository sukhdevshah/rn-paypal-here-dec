import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import CustomKeyboard from 'react-native-keyboard'
import PropTypes from 'prop-types'
import { SCREEN_WIDTH, SCREEN_HEIGHT } from '../constants'
import globalStyles from '../styles/styles.css'

class NumericKeyboard extends Component {
  state = {
    value: ''
  }
  
  static propTypes = {
    prefixText: PropTypes.string,
    onValueChange: PropTypes.func.isRequired,
  }
  
  onClear = () => {
    this.setState({ value: '' }, this.onChange)
  }
  
  onKeyPress = (key) => {
    this.setState({ value: this.state.value + key }, this.onChange)
  }
  
  onDelete = () => {
    this.setState({ value: this.state.value.slice(0, -1) }, this.onChange)
  }
  
  onChange() {
    this.props.onValueChange(this.state.value)
  }
  
  render() {
    const prefixText = this.props.prefixText
    return (
      <View style={ styles.container }>
        <Text style={ globalStyles.textContainer }>{ `${ prefixText ? prefixText : ''}${this.state.value}` }</Text>
        
        <View style={ styles.keyboardContainer }>
          <CustomKeyboard
            fullBorder={ true }
            onClear={ this.onClear }
            onKeyPress={ this.onKeyPress }
            onDelete={ this.onDelete }
            keyboardType={ this.props.type } />
        </View>
      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1,
    width: SCREEN_WIDTH / 2,
  },
  keyboardContainer: {
    marginVertical: SCREEN_HEIGHT / 20
  },
}

export default NumericKeyboard
