import React from 'react'
import { Text, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import { SCREEN_WIDTH, SCREEN_HEIGHT } from '../constants'
import { COLOR_PRIMARY, COLOR_TEXT_BUTTON } from '../constants/colors'

const propTypes = {
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  type: PropTypes.string,
  style: PropTypes.object,
}

const Button = ({ title, onPress, type, style }) => {
  return (
    <TouchableOpacity
      style={[
        styles.primaryButton,
        style
      ]}
      onPress={ onPress }>
        <Text style={[
          styles.primaryButtonText ,
          type === 'secondary' ? styles.secondaryButtonText : {},
        ]}>
          { title }
        </Text>
      </TouchableOpacity>
  )
}

Button.propTypes = propTypes

const styles = {
  primaryButton: {
    backgroundColor: COLOR_PRIMARY,
    paddingVertical: SCREEN_HEIGHT / 30,
    paddingHorizontal: SCREEN_WIDTH / 15,
    marginVertical: SCREEN_HEIGHT / 30,
  },
  primaryButtonText: {
    backgroundColor: 'transparent',
    fontSize: SCREEN_HEIGHT / 20,
    color: COLOR_TEXT_BUTTON,
    textAlign: 'center',
    fontFamily: 'FranklinGothicStd-ExtraCond',
  },
  secondaryButtonText: {
    fontFamily: 'FranklinGothic-Roman'
  },
}

export default Button
