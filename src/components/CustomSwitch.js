//@flow
import React from 'react'
import { View, Text, Switch } from 'react-native'
import PropTypes from 'prop-types'
import { SCREEN_HEIGHT } from '../constants'
import { COLOR_TEXT_PRIMARY } from '../constants/colors'

const propTypes = {
  onSwitchChange: PropTypes.func.isRequired,
  value: PropTypes.bool,
}

const CustomSwitch = ({ value, onSwitchChange }) => {
  return (
    <View style={ styles.switchContainer }>
      <Text style={ styles.switchText }>{ `Sandbox ${value ? 'ON' : 'OFF'}` }</Text>
      <Switch
        style={ styles.switch }
        value={ value }
        onValueChange={ value => { onSwitchChange(value) } } />
    </View>
  )
}

CustomSwitch.propTypes = propTypes

const styles = {
  switchContainer: {
    marginTop: SCREEN_HEIGHT / 40,
    alignItems: 'center',
  },
  switch: {
    marginTop: SCREEN_HEIGHT / 100,
  },
  switchText: {
    color: COLOR_TEXT_PRIMARY,
    fontFamily: 'OpenSans',
  },
}

export default CustomSwitch
