import React from 'react'
import { View } from 'react-native'
import PropTypes from 'prop-types'

const propTypes = {
  children: PropTypes.node.isRequired,
}

const Footer = ({ children }) =>
  <View style={ styles.container }>
    { children }
  </View>

Footer.propTypes = propTypes

const styles = {
  container: {
    flex: 1,
    position: 'absolute',
    bottom: 0,
  },
}

export default Footer
