import React from 'react'
import { Text } from 'react-native'
import PropTypes from 'prop-types'
import { SCREEN_WIDTH, SCREEN_HEIGHT } from '../constants'
import { COLOR_PARAGRAPH } from '../constants/colors'

const propTypes = {
  text: PropTypes.string.isRequired,
}

const Paragraph = ({ text }) => {
  return (
    <Text style={ styles.text } >{ text }</Text>
  )
}

Paragraph.propTypes = propTypes

const styles = {
  text: {
    fontSize: SCREEN_HEIGHT / 30,
    color: COLOR_PARAGRAPH,
    backgroundColor: 'transparent',
    marginBottom: SCREEN_HEIGHT / 35,
    textAlign: 'center',
    marginHorizontal: SCREEN_WIDTH / 10,
    fontFamily: 'OpenSans',
  },
}

export default Paragraph
