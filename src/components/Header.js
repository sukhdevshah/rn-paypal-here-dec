import React from 'react'
import { View, Image, Button, NativeModules } from 'react-native'
import PropTypes from 'prop-types'
import Navigator from 'native-navigation'
import { SCREEN_WIDTH, SCREEN_HEIGHT } from '../constants'
import { COLOR_BACK, COLOR_CANCEL } from '../constants/colors'

const { PayPalHere } = NativeModules

const propTypes = {
  backHidden: PropTypes.bool,
  cancelHidden: PropTypes.bool,
}

const Header = ({ backHidden, cancelHidden }) => {
  const onCancel = () => {
    if (cancelHidden) { return }

    PayPalHere.cancelTransaction()
    Navigator.present('AmountSelection')
  }

  const onBack = () => {
    if (backHidden) { return }

    Navigator.pop()
  }

  return (
    <View style={ styles.container }>
      <View style={[
        styles.flexContainer,
        {
          alignItems: 'flex-start',
        }
      ]}>
        <Button
          title={ 'Back' }
          color={ backHidden ? 'transparent' : COLOR_BACK }
          onPress={ onBack } />
      </View>
      <View style={[
        styles.flexContainer,
        {
          alignItems: 'center',
        }
      ]}>
        <Image
          style={ styles.logo }
          source={ require('../images/theme/logo.png') } />
      </View>
      <View style={[
        styles.flexContainer,
        {
          alignItems: 'flex-end',
        }
      ]}>
        <Button
          color={ cancelHidden ? 'transparent' : COLOR_CANCEL }
          title={ 'Cancel' }
          onPress={ onCancel } />
      </View>
    </View>
  )
}

Header.propTypes = propTypes

const styles = {
  container: {
    flex: 1,
    flexDirection: 'row',
    marginHorizontal: SCREEN_WIDTH / 50,
  },
  logo: {
    resizeMode: 'contain',
    height: SCREEN_HEIGHT / 12,
  },
  flexContainer: {
    flex: 1,
    justifyContent: 'center',
  },
}

export default Header
