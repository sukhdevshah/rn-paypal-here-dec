import React from 'react'
import { Text } from 'react-native'
import PropTypes from 'prop-types'
import { SCREEN_HEIGHT } from '../constants'
import { COLOR_HEADING } from '../constants/colors'

const propTypes = {
  text: PropTypes.string.isRequired,
}

const Heading = ({ text }) => {
  return (
    <Text style={ styles.text } >{ text }</Text>
  )
}

Heading.propTypes = propTypes

const styles = {
  text: {
    fontSize: SCREEN_HEIGHT / 20,
    color: COLOR_HEADING,
    textAlign: 'center',
    marginBottom: SCREEN_HEIGHT / 50,
    fontFamily: 'OpenSans-Bold',
  },
}

export default Heading
