import React, { Component } from 'react'
import { View, Image } from 'react-native'
import PropTypes from 'prop-types'
import Navigator from 'native-navigation'
import Header from './Header'
import Loading from '../components/Loading'
import { SCREEN_WIDTH } from '../constants'
import * as ROUTE from '../constants/routes'

const propTypes = {
  children: PropTypes.node.isRequired,
  isLoading: PropTypes.bool,
  status: PropTypes.string,
}

const contextTypes = {
  nativeNavigationInstanceId: PropTypes.string,
}

class Screen extends Component {
  render() {
    const { children } = this.props
    const navigationInstanceId = this.context.nativeNavigationInstanceId
    const navigationOptions = navigationInstanceId.includes(ROUTE.LOGIN) ||
    navigationInstanceId.includes(ROUTE.AMOUNTSELECTION) && !navigationInstanceId.includes(ROUTE.MANUALAMOUNTSELECTION) || navigationInstanceId.includes(ROUTE.THANKYOU) || navigationInstanceId.includes(ROUTE.CANCELLED)

    return (
      <Navigator.Config
        hidden
      >
        <View style={[ styles.mainContainer, isSandbox() ]}>
          <Image
            style={ styles.backgroundImage }
            source={ require('../images/theme/background.png') }>

              { this.props.isLoading && <Loading status={ this.props.status } /> }

            <Header
              backHidden={ navigationOptions }
              cancelHidden={ navigationOptions } />
            <View style={ styles.container }>
              {children}
            </View>
          </Image>
        </View>
      </Navigator.Config>
    )
  }
}

function isSandbox() {
  let styles = {}

  if (global.sandbox) {
    styles = {
      borderColor: 'red',
      borderWidth: 3,
      borderStyle: 'solid',
    }
  }

  return styles
}

Screen.propTypes = propTypes
Screen.contextTypes = contextTypes

const styles = {
  mainContainer: {
    flex: 5,
  },
  container: {
    flex: 5,
    alignItems: 'center',
    marginHorizontal: SCREEN_WIDTH / 8,
  },
  backgroundImage: {
    width: '100%',
    flex: 5,
    backgroundColor: 'transparent',
    resizeMode: 'cover',
  },
}

export default Screen
