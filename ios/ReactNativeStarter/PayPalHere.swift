//
//  PayPalHereModule.swift
//  ReactNativeStarter
//
//  Created by Sukhdev Shah on 20/07/2017.
//  Copyright © 2017 Facebook. All rights reserved.
//

import Foundation
import NativeNavigation
import React

@objc(PayPalHere)
class PayPalHere: RCTEventEmitter, PPHTransactionControllerDelegate, PPHTransactionManagerDelegate {
  
  var transactionWatcher: PPHTransactionWatcher!
  var amount: PPHAmount!
  var invoice: PPHInvoice!
  
  override init() {
    super.init()
    self.transactionWatcher = PPHTransactionWatcher.init(delegate: self)
  }
  
  func askForLocationAccess() -> Void {
    PayPalHereSDK.askForLocationAccess()
  }
  
  func setSandboxEnvironment() -> Void {
    PayPalHereSDK.selectEnvironment(with: PPHSDKServiceType.ePPHSDKServiceType_Sandbox)
  }
  
  func setupWithCredentials(_ accesss_token: String, refresh_url: String, expiry: String) -> Void {
    PayPalHereSDK.setup(withCredentials: accesss_token, refreshUrl: refresh_url, tokenExpiryOrNil: expiry, thenCompletionHandler: { (status: PPHInitResultType, error: PPHError?, info: PPHMerchantInfo?)
      in
      
      switch (status) {
        
      case PPHInitResultType.ePPHInitResultSuccess:
        self.sendEvent(withName: "PayPalInitSuccess", body: "")
        
      case PPHInitResultType.ePPHInitResultFailedInsufficientCredentials:
        self.sendEvent(withName: "PayPalInitInsufficientCredentials", body: error?.mappedMessage())
        
      case PPHInitResultType.ePPHInitResultFailed:
        self.sendEvent(withName: "PayPalInitFail", body: error?.mappedMessage())
      }
    })
  }
  
  func _createInvoice(_ item: String, amount: PPHAmount) {
    self.invoice = PPHInvoice.init(item: item, for: amount)
  }
  
  func setupReader(_ item: String, amount: String) {
    self.amount = PPHAmount.init(string: amount)
    self.invoice?.removeAllItems()
    _createInvoice(item, amount: self.amount)
    PayPalHereSDK.sharedTransactionManager().beginPaymentUsingUI(with: self.invoice, transactionController: self)
  }
  
  func enableContactlessReader() {
    PayPalHereSDK.sharedTransactionManager().activateReader(forPayments: nil)
  }
  
  func cancelTransaction() -> PPHError? {
    PayPalHereSDK.sharedTransactionManager().deActivateReaderForPayments()
    return PayPalHereSDK.sharedTransactionManager().cancelPayment()
  }

  
  
  // RCTBridgeModule Protocol
  override func supportedEvents() -> [String]! {
    return ["PayPalInitSuccess", "PayPalInitInsufficientCredentials", "PayPalInitFail", "PayPalPaymentResponseSuccess", "PayPalPaymentResponseFailed", "PayPalPaymentReaderIdle", "PayPalPaymentGettingInfo", "PayPalPaymentStartReaderDetection", "PayPalPaymentDidDetectReader", "PayPalPaymentReaderRemoved", "PayPalPaymentCardReadBegun", "PayPalPaymentCardDataReceived", "PayPalPaymentReadCardNotAllowed", "PayPalPaymentFailedToReadCard", "PayPalPaymentProcessing", "PayPalPaymentWaitingForSignature", "PayPalPaymentCancelled", "PayPalPaymentDeclined"]
  }
  
  

  // PPHTransactionControllerDelegate Protocol
  func userDidSelect(_ paymentMethod: PPHPaymentMethod) -> Void {
    PayPalHereSDK.sharedTransactionManager().processPaymentUsingUI(withPaymentType: paymentMethod) { (response: PPHTransactionResponse?) in
      
      var view: ReactViewController
      
      if ((response?.error) != nil) {
        
        self.sendEvent(withName: "PayPalPaymentResponseFailed", body: response?.error.mappedMessage())
        
        view = ReactViewController.init(moduleName: "Cancelled")
      } else {
        
        let record = response?.record
        var invoice = record?.invoice.asDictionary()
        invoice?["amount"] = self.amount.stringValueWithoutCurrencySymbol()
        invoice?["paidWithPayPal"] = record?.paidWithPayPal
        invoice?["payPalInvoiceId"] = record?.payPalInvoiceId
        invoice?["transactionId"] = record?.transactionId
        invoice?["transactionStatus"] = record?.transactionStatus.rawValue
        
        self.sendEvent(withName: "PayPalPaymentResponseSuccess", body: invoice)
        
        view = ReactViewController.init(moduleName: "ThankYou")
      }
      
      ReactNavigationCoordinator.sharedInstance.topNavigationController()?.pushReactViewController(view, animated: true)
    }
  }
  
  func userDidSelectRefundMethod(_ refundMethod: PPHPaymentMethod) -> Void {
    
  }
  
  func getCurrentNavigationController() -> UINavigationController {
    return ReactNavigationCoordinator.sharedInstance.topNavigationController()!
  }
  
  func receiptOptionsWillAppear(for transactionRecord: PPHTransactionRecord) -> Void {
    print("----------------------------------------------------------------------")
    print("Receipt Options")
    print(transactionRecord)
    print("----------------------------------------------------------------------")
  }
  
//  func contactlessTimeoutAction() -> PPHContactlessTimeoutAction {
//    <#code#>
//  }
  
  
  
  // PPHTransactionManagerDelegate Protocol
  func onPaymentEvent(_ event: PPHTransactionManagerEvent!) -> Void {
    let description = event.description
    
    switch(event.eventType) {
      
    case PPHTransactionEventType.ePPHTransactionType_Idle:
      self.sendEvent(withName: "PayPalPaymentReaderIdle", body: description)
      
    case PPHTransactionEventType.ePPHTransactionType_GettingPaymentInfo:
      self.sendEvent(withName: "PayPalPaymentGettingInfo", body: description)
      
    case PPHTransactionEventType.ePPHTransactionType_DidStartReaderDetection:
      self.sendEvent(withName: "PayPalPaymentStartReaderDetection", body: description)
      
    case PPHTransactionEventType.ePPHTransactionType_DidDetectReaderDevice:
      self.sendEvent(withName: "PayPalPaymentDidDetectReader", body: description)
      
    case PPHTransactionEventType.ePPHTransactionType_DidRemoveReader:
      self.sendEvent(withName: "PayPalPaymentReaderRemoved", body: description)
      
    case PPHTransactionEventType.ePPHTransactionType_CardReadBegun:
      self.sendEvent(withName: "PayPalPaymentCardReadBegun", body: description)
      
    case PPHTransactionEventType.ePPHTransactionType_CardDataReceived:
      self.sendEvent(withName: "PayPalPaymentCardDataReceived", body: description)
      
    case PPHTransactionEventType.ePPHTransactionType_ReadCardNotAllowed:
      self.sendEvent(withName: "PayPalPaymentReadCardNotAllowed", body: description)
      
    case PPHTransactionEventType.ePPHTransactionType_FailedToReadCard:
      self.sendEvent(withName: "PayPalPaymentFailedToReadCard", body: description)
      
    case PPHTransactionEventType.ePPHTransactionType_ProcessingPayment:
      self.sendEvent(withName: "PayPalPaymentProcessing", body: description)
      
    case PPHTransactionEventType.ePPHTransactionType_WaitingForSignature:
      self.sendEvent(withName: "PayPalPaymentWaitingForSignature", body: description)
      
    case PPHTransactionEventType.ePPHTransactionType_TransactionCancelled:
      self.sendEvent(withName: "PayPalPaymentCancelled", body: description)
      
    case PPHTransactionEventType.ePPHTransactionType_TransactionDeclined:
      self.sendEvent(withName: "PayPalPaymentDeclined", body: description)
      
    default: break
    }
  }
}
