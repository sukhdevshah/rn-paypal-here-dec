//
//  PayPalHereModuleExports.m
//  ReactNativeStarter
//
//  Created by Sukhdev Shah on 27/06/2017.
//  Copyright © 2017 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(PayPalHere, NSObject)

RCT_EXTERN_METHOD(askForLocationAccess)

RCT_EXTERN_METHOD(setSandboxEnvironment)

RCT_EXTERN_METHOD(setupWithCredentials:(NSString *)access_token refresh_url:(NSString *)refresh_url expiry:(NSString *)expiry)

RCT_EXTERN_METHOD(setupReader:(NSString *)item amount:(NSString *)amount)

RCT_EXTERN_METHOD(enableContactlessReader)

RCT_EXTERN_METHOD(cancelTransaction)

@end
